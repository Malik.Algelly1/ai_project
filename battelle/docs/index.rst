.. Battelle documentation master file, created by
   sphinx-quickstart on Tue Mar 30 10:12:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Battelle's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Subpackages
---------

.. toctree::
   :maxdepth: 2

   nn
   knn
   perceptron

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
