import setuptools

with open("README.rst", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="battelle",
    version="0.0.1",
    author="Groupe B",
    description="A library for creating neural networks",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.unige.ch/Samuel.Simko/group-b-projet-ia",
    project_urls={
        "Bug Tracker": "https://gitlab.unige.ch/Samuel.Simko/group-b-projet-ia/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"battelle": "src",
                 "battelle.perceptron": "src/perceptron",
                 "battelle.knn": "src/knn",
                 "battelle.nn": "src/nn"},
    packages=['battelle', 'battelle.perceptron', 'battelle.nn', 'battelle.knn'],
    python_requires=">=3.6",
)
