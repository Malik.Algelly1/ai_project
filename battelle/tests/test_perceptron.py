import numpy as np
import matplotlib.pyplot as plt

from battelle.perceptron import Perceptron

# Initialisation de données (augmentées) aléatoires entre -1 et 1
input_dim = 2
x = np.random.uniform(-1,1, size=(400,input_dim))

# Label des données
y = np.array([ 1 if k[0] > -0.5 else 0 for k in x])

# (and)
y = np.array([ 1 if k[1] > 0 and k[0] >= 0 else 0 for k in x])

# Initialisation des poids aléatoirement et du Perceptron
weights = np.array(0.5 - np.random.random(size=(input_dim+1)))
p = Perceptron(weights)

# Entrainement du Perceptron avec les données aléatoires
p.train(x, y)

# Affichage des données aléatoires avec la droite de séparation calculée.
plt.subplot(1,2,1)

# w1*x1 + w2*x2 + w3 = 0 => x2 = (-w1*x1 - w3) / w2
# où w3 est le biais
x01, x02 = np.amin(x[:, 0]), np.amax(x[:, 0])
x11 = (-p.weights[0] * x01 - p.weights[-1]) / p.weights[1]
x12 = (-p.weights[0] * x02 - p.weights[-1]) / p.weights[1]
plt.plot([x01, x02], [x11, x12], "k")

ymin, ymax = np.amin(x[:, 1]), np.amax(x[:, 1])
plt.ylim([ymin-1, ymax+1])

for i, xi in enumerate(x):
    if y[i] == 1:
        plt.plot(xi[0], xi[1], 'or')
    else:
        plt.plot(xi[0], xi[1], 'ob')
plt.title("Testing data")

# Affichage de prédiction de données de test avec le Perceptron entrainé.
x_test = np.array([[i,j] for i in np.linspace(-1, 1, 20) for j in np.linspace(-1, 1, 20)])
plt.subplot(1,2,2)
for [i,j] in x_test:
    if p.predict([i,j]):
        plt.plot(i, j, 'or')
    else:
        plt.plot(i, j, 'ob')
plt.title("Perceptron result")

plt.plot([x01, x02], [x11, x12], "k")
plt.ylim([ymin-1, ymax+1])
plt.xlabel(f"Loss: {np.mean([(p.predict(x[k]) - y[k])**2 for k in range(len(x))])}")


plt.show()
