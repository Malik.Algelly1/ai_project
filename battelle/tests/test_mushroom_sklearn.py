# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn import neural_network as sknn
# from sklearn.metrics import confusion_matrix
from battelle.nn import *

# open mushroom database

data = pd.read_csv("agaricus-lepiota.data", header=None).values
np.random.shuffle(data)

# Replace binary values of first column from 0 to 1
data[:, 0] = [{"p": 0, "e": 1}[x] for x in data[:, 0]]

# Replace each other letter by their alphabetical order (eg a = 0, b = 1/25... z = 1)
data[:, 1:] = np.vectorize(lambda x: (ord(x) - 97) / 25)(data[:, 1:])
training_labels_not_normalize = data[:,0]
data = preprocessing.normalize(data, axis = 0)
data[:,0] = training_labels_not_normalize
# replace each letter by a double from 0-1

[training_data, testing_data] = train_test_split(data, train_size=0.8, random_state = 2022)


#training_data = data[: int(0.7 * N)]
training_labels = training_data[:, 0]
training_labels = [[x] for x in training_labels]
training_data = np.delete(training_data, 0, 1)

#testing_data = data[int(0.7 * N) :]
testing_labels = testing_data[:, 0]
testing_labels = [[x] for x in testing_labels]
testing_data = np.delete(testing_data, 0, 1)

classifier = sknn.MLPClassifier(hidden_layer_sizes=(4,5,1), max_iter=300,activation = 'tanh',solver='adam',random_state=1,learning_rate_init=0.03)

classifier.fit(training_data, training_labels)
y_pred = classifier.predict(testing_data)

y_pred = preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
testing_labels = preprocessing.normalize(testing_labels, axis = 0)

# y_pred = preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
# testing_labels= preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
loss = np.mean((y_pred - testing_labels)**2)

print(loss)