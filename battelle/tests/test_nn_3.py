import numpy as np
from numpy.random import random
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from sklearn.datasets import make_classification

from battelle.nn import NeuralNetwork , Layer
from battelle.knn import KNearestNeighborsClassifier
from battelle.perceptron import Perceptron

# Generate data
INPUT_DIM = 2
CLASSES = 3
x,features = make_classification(
    n_samples=300,
    n_features=INPUT_DIM,
    n_informative=INPUT_DIM,
    n_redundant=0,
    n_repeated=0,
    n_classes=CLASSES,
    n_clusters_per_class=1,
    class_sep=1.0
)

y = []
for f in features :
    temp = [0,0,0]
    temp[f] = 1
    y.append(temp) 
y = np.array(y)    

# Initialize and train the neural network
nn = NeuralNetwork()
nn.add_layer(Layer(5, 'tanh', INPUT_DIM))
nn.add_layer(Layer(4, 'tanh'))
nn.add_layer(Layer(CLASSES, 'tanh'))

NUM_EPOCHS = 100
for i in range(NUM_EPOCHS):
    print(f"Epoch {i+1}/{NUM_EPOCHS}")
    nn.train(x, y, learning_rate=0.03)
    print(f"Loss: {np.mean([(nn.predict(x[k]) - y[k])**2 for k in range(len(x))])}")

# Plot contour 
map_dict = {
    'red':  ((0.0, 0.0, 1.0),
             (0.3333, 1.0, 0.0),
             (1.0, 0.0, 0.0)),

    'green':((0.0, 0.0, 0.0),
             (0.3333, 0.0, 1.0),
             (0.6666, 1.0, 0.0),
             (1.0, 0.0, 0.0)),

    'blue': ((0.0, 0.0, 0.0),
             (0.6666, 0.0, 1.0),
             (1.0, 1.0, 0.0)),

    'alpha':((0.0, 0.0, 0.0),
             (0.3333, 0.7, 0.0),
             (0.6666, 0.7, 0.0),
             (1.0, 0.7, 0.0))
}
map = LinearSegmentedColormap('rgb', map_dict)

RES = 100
min_x = min(x[:,0])
max_x = max(x[:,0])
min_y = min(x[:,1])
max_y = max(x[:,1])

xrange = np.linspace(min_x, max_x, RES)
yrange = np.linspace(min_y, max_y, RES)
z = []
for y_coord in yrange :
    row = []
    for x_coord in xrange :
        p = nn.predict([x_coord, y_coord])
        c = np.argsort(p)[-1]
        row.append((c + p[c])/3.)
    z.append(row)
plt.imshow(np.rot90(np.transpose(z)), cmap=map, extent=(min_x,max_x,min_y,max_y))

# Plot training data
for i, xi in enumerate(x):
    if features[i] == 0:
        plt.plot(xi[0], xi[1], '.r')
    elif features[i] == 1:
        plt.plot(xi[0], xi[1], '.g')
    else:
        plt.plot(xi[0], xi[1], '.b')


plt.show()
