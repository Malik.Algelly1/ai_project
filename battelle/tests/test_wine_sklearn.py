# -*- coding: utf-8 -*-
#import random

import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn import neural_network as sknn
from sklearn.metrics import confusion_matrix

from battelle.nn import *

# open mushroom database
data = pd.read_csv("../data/winequality-white.csv", delimiter = ";").values
data = np.vstack((deepcopy(data[:,-1]),data.T)).T #Pour mettre les labels dans la première colonne
data = np.delete(data, -1, 1)
training_labels = data[:, 0]
data = preprocessing.normalize(data, axis = 0)
data[:,0] = training_labels

#print(data[:,0])

[training_data, testing_data] = train_test_split(data, train_size=0.3, random_state = 2022)

#training_data = data[: int(0.7 * N)]
training_labels = training_data[:, 0]
#training_labels = [[x] for x in training_labels]
training_data = np.delete(training_data, 0, 1)

#testing_data = data[int(0.7 * N) :]
testing_labels = testing_data[:, 0]
testing_labels = [[x] for x in testing_labels]
testing_data = np.delete(testing_data, 0, 1)

classifier = sknn.MLPClassifier(hidden_layer_sizes=(4,5,1), max_iter=300,activation = 'tanh',solver='adam',random_state=1,learning_rate_init=0.03)

# classifier2 = sknn.MLPRegressor(hidden_layer_sizes=(4,5,1), max_iter=300,activation = 'tanh',solver='adam',random_state=1,learning_rate_init=0.3)

classifier.fit(training_data, training_labels)
y_pred = classifier.predict(testing_data)

y_pred = preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
testing_labels = preprocessing.normalize(testing_labels, axis = 0)

# y_pred = preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
# testing_labels= preprocessing.normalize(y_pred.reshape(-1,1), axis = 0)
loss = np.mean((y_pred - testing_labels)**2)

print(loss)




# nn = NeuralNetwork()
# nn.add_layer(Layer(4, "sigmoid", training_data.shape[1]))
# nn.add_layer(Layer(5, "sigmoid"))
# nn.add_layer(Layer(1, "sigmoid"))

# #NUM_EPOCHS = 20
# old_loss = 200
# loss = 100
# i = 0
# while old_loss / loss > 1.01:
#     print(f"Epoch {i+1}/?")
#     nn.train(training_data, training_labels, learning_rate=0.03)
#     old_loss = deepcopy(loss)
#     loss = np.mean([(np.array(nn.predict(testing_data[k])) - testing_labels[k][0])**2 for k in range(len(testing_labels))])
#     print(
#         f"Loss: {loss}"
#     )
#     i += 1
# nn.print_weights()
# a = nn.all_weights
# v_abs = np.vectorize(abs)
# for j,i in enumerate(a) : 
#     plt.matshow(v_abs(i))
#     plt.title("Wine dataset : Couche " + str(j))
    
