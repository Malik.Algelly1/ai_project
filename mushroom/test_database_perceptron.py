# -*- coding: utf-8 -*-
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Perceptron(object):
    """A Perceptron neuron

    Attributes
    ----------
    weights : array
        The initial weights of the neuron

    """

    def __init__(self, weights):
        self.__weights = weights

    @property
    def weights(self):
        return self.__weights

    @weights.setter
    def weights(self, weights):
        self.__weights = weights

    def train(self, x, y, learning_rate=0.01, nb_iter=200):
        for _ in range(nb_iter):
            for i, xi in enumerate(x):
                tmp_y = 1 if np.dot(xi, self.weights) >= 0 else 0
                self.weights = self.weights + (learning_rate * (y[i] - tmp_y)) * xi

    def predict(self, x):
        return 1 if np.dot(x, self.weights) >= 0 else 0

# open mushroom database
data = pd.read_csv("agaricus-lepiota.data", header=None).values
np.random.shuffle(data)

# Replace binary values of first column from 0 to 1
data[:, 0] = [{"p": 0, "e": 1}[x] for x in data[:, 0]]

# Replace each other letter by their alphabetical order (eg a = 0, b = 1/25... z = 1)
data[:, 1:] = np.vectorize(lambda x: (ord(x) - 97) / 25)(data[:, 1:])

# replace each letter by a double from 0-1
N = data.shape[0]

training_data = data[: int(0.7 * N)]
training_labels = training_data[:, 0]
training_labels = [x for x in training_labels]
training_data[:,0] = 1

testing_data = data[int(0.7 * N) :]
testing_labels = testing_data[:, 0]
testing_labels = [x for x in testing_labels]
testing_data[:,0] = 1

input_dim = data.shape[1]
weights = np.zeros(input_dim)
p = Perceptron(weights)

NUM_EPOCHS = 20
for i in range(NUM_EPOCHS):
    print(f"Epoch {i+1}/{NUM_EPOCHS}")
    p.train(training_data, training_labels, learning_rate=0.03, nb_iter=1)
    print(
        f"Loss: {np.mean([(np.array(p.predict(testing_data[k])) - testing_labels[k])**2 for k in range(len(testing_labels))])}"
    )
