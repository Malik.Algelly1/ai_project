# -*- coding: utf-8 -*-
import requests

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/"
files = ["agaricus-lepiota.data", "agaricus-lepiota.names"]

for filename in files:
    r = requests.get(url + filename, allow_redirects=True)
    with open(filename, "wb") as f:
        f.write(r.content)
